#ifndef ERROR_MESSAGE_HPP
#define ERROR_MESSAGE_HPP

#include <QMessageBox>
#include <QLineEdit>
#include <vector>

class ErrorMessage {
public:
    ~ErrorMessage() = default;

    ErrorMessage(const std::vector<QLineEdit*> lineEdits, const std::vector<QString> labelTexts):
        m_lineEdits(lineEdits), m_labelTexts(labelTexts)
    {

    }

    bool hasTypeError() {
        checkForInvalidInput();
        return m_typeError.length() > 0;
    }

    bool hasInputError() {
        return checkForEmptyInput();
    }


    void displayEmptyInputMessage()
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle("Error");
        msgBox.setInformativeText("Please check your inputs, fields cannot be empty!");
        msgBox.exec();
    }

    void displayInvalidInputMessage() {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle("Invalid input");
        msgBox.setInformativeText(QString::fromStdString(m_typeError));
        msgBox.exec();
    }

    void displayWarningMessage(std::string msg)
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle("Warning");
        msgBox.setInformativeText(QString::fromStdString(msg));
        msgBox.exec();
    }

private:
    std::string m_typeError = "";

    const std::vector<QLineEdit*> m_lineEdits;
    const std::vector<QString> m_labelTexts;

    void checkForInvalidInput()
    {
        bool ok;
        int i = 0;
        for(auto line : m_lineEdits) {
            line->text().toDouble(&ok);
            if(!ok) {
                m_typeError += m_labelTexts.at(i).toStdString() + " must be a number.\n";
            }
            i += 1;
        }

    }

    bool checkForEmptyInput()
    {
        for(auto line : m_lineEdits) {
            if(line->text().length() <= 0) {
                return true;
            }
        } return false;
    }

};




#endif // ERROR_MESSAGE_HPP
