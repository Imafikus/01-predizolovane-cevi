#ifndef LCOMPESATION
#define LCOMPESATION

#include <string>
#include <cmath>
#include <fstream>
#include <unordered_map>

#include "Pipe.hpp"
#include "SinglePipe.hpp"
#include "TwinPipe.hpp"
#include "Project.hpp"
#include "FrictionalLength.hpp"

class Lcompesation : public FrictionalLength
{
public:
	
	Lcompesation(double FlowTemp, double ReturnTemp, double InstallTemp, double SoilDensity, double InternalSoilFrictionAngle,
                 double DesignWaterPressure, int type, int Series, int DN, double CoverDepth, double L1, double L2, double angle,
                 std::string Name = "", std::string CalcRefNum = "", std::string CarriedOutBy = "", std::string Date = "")
		: FrictionalLength(FlowTemp, ReturnTemp, InstallTemp, SoilDensity, InternalSoilFrictionAngle, DesignWaterPressure,
        	type, Series, DN, CoverDepth, Name, CalcRefNum, CarriedOutBy, Date), m_L1(L1), m_L2(L2), m_angle(angle)
		{
        }
    // Da li je potrebno predgrevanje
    bool Preheating(double L)
    {
    	double flength = FrictionLength();
    	return L > flength;
    }
	// kolona AO
	double SlidingSectionLength(double L)
	{
		double fforce = FrictionForce();
		double temp = m_FlowTemp;
		if (Preheating(L))
			temp = m_PrestressingTemp;
		
		if (m_type == 1)
		{
			double f1 = m_ThermalElongationCoeff*m_YoungsFlexibilityModulus*(temp-m_InstallTemp)+m_SinglePipes[m_DN].t();
			return f1*m_SinglePipes[m_DN].SectionalArea()/fforce;
		}
		else
		{
			double f1 = m_ThermalElongationCoeff*m_YoungsFlexibilityModulus*((temp+m_ReturnTemp)/2-m_InstallTemp);
			return (f1+m_TwinPipes[m_DN].t())*m_TwinPipes[m_DN].SectionalArea()/fforce;
		}
	}
	// kolona AP
	double SlidingZone(double L)
	{
		double ssl = SlidingSectionLength(L);
		double temp = m_FlowTemp;
		if (Preheating(L))
			temp = m_PrestressingTemp;
		
		if (m_type == 1)
		{
			double f1 = m_ThermalElongationCoeff*(temp-m_InstallTemp);
			return (f1+m_SinglePipes[m_DN].t()/m_YoungsFlexibilityModulus)*ssl/2*1000;
		}
		else
		{
			double f1 = m_ThermalElongationCoeff*((temp+m_ReturnTemp)/2-m_InstallTemp);
			return (f1+m_TwinPipes[m_DN].t()/m_YoungsFlexibilityModulus)*ssl/2*1000;
		}
	}
	//kolona AQ
	double Expansion(double L)
	{
		double flength = FrictionLength();
		double ssl = SlidingSectionLength(L);
		double sz = SlidingZone(L);
		
		if (L < flength)
			return sz*L/ssl*(2-L/ssl);
		else
			return sz;
	}
	// kolona CG
	double DeltaExpansion(double L1, double L2)
	{
		double e1 = Expansion(L1);
		double e2 = Expansion(L2);
		
		return e1/sin(m_angle*PI/180) + e2/tan(m_angle*PI/180);
	}
	
	// kolona CH
	double DilatationZone(double L1, double L2)
	{
		double de = DeltaExpansion(L1, L2);
		double carrier;
		
		if (m_type == 1)
			carrier = m_SinglePipes[m_DN].Carrier();
		else
			carrier = m_TwinPipes[m_DN].Carrier();
		
		return 1.15*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)/1000*sqrt(de*carrier);
	}
	//kolona CQ
	double DeltaL()
	{
		double de1 = DeltaExpansion(m_L1, m_L2);
		double de2 = DeltaExpansion(m_L2, m_L1);
		
		return sqrt(de1*de1+de2*de2);
	}
	//kolona CK
	double FoamPads3()
	{
		double dl = DeltaL();
		double carrier;
		
		if (m_type == 1)
			carrier = m_SinglePipes[m_DN].Carrier();
		else
			carrier = m_TwinPipes[m_DN].Carrier();
		
		if (dl > 54)
			return ceil(1/1000.0*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)*(sqrt(dl*carrier)-sqrt(54*carrier)));
		return 0;
	}
	//kolona CJ
	double FoamPads2()
	{
		double dl = DeltaL();
		double fp3 = FoamPads3();
		double carrier;
		
		if (m_type == 1)
			carrier = m_SinglePipes[m_DN].Carrier();
		else
			carrier = m_TwinPipes[m_DN].Carrier();
		
		if (dl > 27)
			return ceil(1/1000.0*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)*(sqrt(dl*carrier)-sqrt(27*carrier)-fp3));
		return 0;
	}
	//kolona CI
	double FoamPads1(double L1, double L2)
	{
		double dl = DeltaL();
		double dz = DilatationZone(L1, L2);
		double fp3 = FoamPads3();
		double fp2 = FoamPads2();
		
		if (dl <= 27)
			return ceil(dz);
		if (dz-(fp2+fp3) > 0)
			return ceil(dz-(fp2+fp3));
		return 0;
	}
	//ovo nema u tabelama
	double SigmaMax(double L)
	{
		double fforce = FrictionForce();
		double sa;
		double l = L;
		if (Preheating(L))
			l = FrictionLength();
		
		if (m_type == 1)
			sa = m_SinglePipes[m_DN].SectionalArea();
		else
			sa = m_TwinPipes[m_DN].SectionalArea();
		
		return (l*fforce)/sa;
	}
	//upozorenje za prevelike diletacije
    std::string WarningDiletation()
	{
        std::string msg = "";
		double d = DeltaL();
	
		if (d >= 81)
		{
            msg = "Warning Diletation too high\n";
		}
        return msg;
	}
    //upozorenje za predgrevanje
    std::string WarningPreheating()
    {
        std::string msg = "";
        if (Preheating(m_L1))
        {
            msg = "Warning preheating is needed on L1\n";
        }
        if (Preheating(m_L2))
        {
            msg = "Warning preheating is needed on L2\n";
        }
        if (Preheating(m_L1) && Preheating(m_L2))
        {
            msg = "Warning preheating is needed on L1 and L2\n";
        }
        return msg;
    }
	//geteri
	double L1() const { return m_L1; }
	double L2() const { return m_L2; }
	double angle() const { return m_angle; }
	
	//seteri
	void setL1(double L1) { m_L1 = L1; }
	void setL2(double L2) { m_L2 = L2; }
	void setAngle(double angle) { m_angle = angle; }
	
protected:
	
	double m_L1;
	double m_L2;
	double m_angle;
};

#endif
