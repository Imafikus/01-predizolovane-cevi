#ifndef SINGLEPIPE_HPP
#define SINGLEPIPE_HPP

#include "Pipe.hpp"
#include <cmath>
#define PI 3.141592653589793238463

class SinglePipe : public Pipe
{
public:
    SinglePipe(int DN, double Carrier, double s, int Series1, int Series2, int Series3, 
    		   double s1, double s2, double s3, int A, int e, int p1, int p2, double DesignWaterPressure,
               double I = 0, double II = 0, double III = 0)
        : Pipe(DN, Carrier, s, Series1, Series2, s1, s2, DesignWaterPressure), 
        m_Series3(Series3), m_s3(s3), m_A(A), m_e(e), m_p1(p1), m_p2(p2), m_I(I), m_II(II), m_III(III)
    {
    }
    SinglePipe() = default;
    ~SinglePipe() = default;
    
    double SectionalArea() const override {
    	return (m_Carrier - m_s) * PI * m_s;
    }
    
    double sigma_up() const override
    {
    	return (m_Carrier / m_s - 1) * m_DesignWaterPressure / 2;
    }
    
    double sigma_lp() const override
    {
    	return m_DesignWaterPressure * pow(m_Carrier - 2*m_s, 2) * 0.785 / SectionalArea();
    }
    
    double t() const override
    {
    	return sigma_lp() - (sigma_up() * 0.3);
    }
    
    double water_per_meter() const override
    {
    	return pow((m_Carrier - 2*m_s)/2, 2) * PI / 1000;
    }
    
    int Series3() const { return m_Series3; }
    double s3() const { return m_s3; }
    int A() const { return m_A; }
    int e() const { return m_e; }
    int p1() const { return m_p1; }
    int p2() const { return m_p2; }
    double I() const { return m_I; }
    double II() const { return m_II; }
    double III() const { return m_III; }

private:
	//precnik spoljne cevi za klasu izolacije 3   			    --- kolona H
	int m_Series3;
	//debljina zida plasticne cevi                 			    --- kolona N
	double m_s3;
	//rastojanje izmedju 2 cevi tepelne ztraty i ztraty tabele  --- kolona O
	int m_A;
	//etazno i paralelna ogranicenja polstare V i E komp tabele --- kolone O, P, Q
	int m_e;
	int m_p1;
	int m_p2;
    double m_I;
    double m_II;
    double m_III;
};

#endif
