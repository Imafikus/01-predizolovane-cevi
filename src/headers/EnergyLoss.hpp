#ifndef ENERGYLOSS_HPP
#define ENERGYLOSS_HPP

#include <string>
#include <cmath>
#include <fstream>
#include <unordered_map>

#include "Pipe.hpp"
#include "SinglePipe.hpp"
#include "TwinPipe.hpp"
#include "Project.hpp"

double R_0 = 0.0685;
double lambda_i = 0.027;
double crownsToEuro = 26.2;

enum Wetness{dry, medium, wet};

class EnergyLoss : public Project
{
public:

	EnergyLoss(double FlowTemp, double ReturnTemp, double InstallTemp, double SoilDensity, double InternalSoilFrictionAngle,
                     double DesignWaterPressure, int type, int Series, int DN, double CoverDepth, int number, int LengthTrasy, Wetness wetness,
                     double CreditInterest, double InflationRate, double PriceRiseDueToEnergy, int UsageTime, double ProductCosts, int TimeFactor,
                     std::string Name = "", std::string CalcRefNum = "", std::string CarriedOutBy = "", std::string Date = "")
		: Project(FlowTemp, ReturnTemp, InstallTemp, SoilDensity, InternalSoilFrictionAngle, DesignWaterPressure,
                  Name, CalcRefNum, CarriedOutBy, Date), m_type(type), m_Series(Series), m_DN(DN), m_CoverDepth(CoverDepth),
                   m_Number(number), m_LengthTrasy(LengthTrasy), m_Wetness(wetness), m_CreditInterest(CreditInterest), m_InflationRate(InflationRate),
                   m_PriceRiseDueToEnergy(PriceRiseDueToEnergy), m_UsageTime(UsageTime), m_ProductCosts(ProductCosts), m_TimeFactor(TimeFactor)
		{  
        }

	~EnergyLoss() = default;
	

    double i_z(){
        return (Z_k()-R_i()-P_e())/100.0;
    }

    double A_f(){
        return (pow(1+i_z(), n()) - 1)/(i_z()  * pow(1+i_z(), n())*1.0);
    }

    double Q_bw(){
        return q()*Q_pk()*this->t()*24.0 * A_f()*m_LengthTrasy/1000.0;
    }

    double K(){
        return Q_bw() + sum_ik();
    }

    double T_s(){
        return (FlowTemp() + ReturnTemp())/2.0;
    }

    double T_a(){
        return (FlowTemp() - ReturnTemp())/2.0;
    }

    double h_s_inv(){
        if (m_type == 1){
            return log(4.0*Z_c()/D_i()) + beta() + log(sqrt(1 + pow(2*Z_c()/C(), 2)));
        }else if(m_type == 2){
            double s1 = 2.0*(lambda_i/lambda_s())*log(4.0*Z_c()/D_i());
            double s2 = log(pow(D_i(), 2)/(2.0*C()*d_o()));
            double s3 = sigma()*log(pow(D_i(), 4)/(pow(D_i(), 4) - pow(C(), 4)));
            double s4_num_sqrt = d_o()/(2.0*C()) - (2*sigma()*d_o()*pow(C(), 3))/(pow(D_i(), 4) - pow(C(), 4) );
            double s4_denom = 1 + pow( d_o()/( 2.0*C() ), 2) + sigma() * pow( (2.0*d_o()*pow( D_i(), 2) * C()) / (pow(D_i(), 4) - pow(C(), 4)) , 2);
            double s4 = pow(s4_num_sqrt, 2)/s4_denom;
            double result =  s1 + s2 + s3 - s4  ;
            return result;
        }
    }

    double h_a_inv(){
        if(m_type == 1){
            return log(4.0*Z_c()/D_i()) + beta() - log(sqrt(1 + pow(2*Z_c()/C(), 2)));
        }else if(m_type == 2){
            double s1 = log(2.0*C()/d_o());
            double s2 = sigma()*log((pow(D_i(), 2) + pow(C(), 2)) / (pow(D_i(), 2) - pow(C(), 2)));
            double s3_num = pow( d_o()/(2.0*C()) - gamma()*C()*d_o()/(16.0*pow(Z_c(), 2)) + 2.0*sigma()*d_o()*pow(D_i(), 2)*C()/(pow(D_i(), 4) - pow(C(), 4)) , 2);
            double s3_denom = 1 - pow(d_o()/(2*C()), 2) - gamma()*d_o()/(4.0*Z_c()) + 2.0*sigma()*pow(d_o(), 2) * pow(D_i(), 2) * (pow(D_i(), 4) + pow(C(), 4))/ pow(pow(D_i(), 4) - pow(C(), 4), 2);
            double s3 = s3_num/ s3_denom;
            double s4 = gamma()*pow(C()/(4.0*Z_c()), 2);
            double result = s1 + s2 - s3 - s4;
            return result;
        }
    }

    std::string WarningUsageTime()
    {
        std::string msg = "";

        if (m_UsageTime > 30 || m_UsageTime < 0)
        {
            msg = "Warning Usage time is out of bounds (0-30 years)\n";
        }
        return msg;
    }

    std::string WarningTimeFactor()
    {
        std::string msg = "";

        if (m_TimeFactor > 365 || m_TimeFactor < 0)
        {
            msg = "Warning Time factor is out of bounds (0-365 days)\n";
        }
        return msg;
    }

    double q_s(){
        if(m_type == 1)
            return (T_s() - t_s())*2.0*PI*lambda_s()*h_s();
        else if (m_type == 2)
            return (T_s() - t_s())*2.0*PI*lambda_i*h_s();
    }

    double q_a(){
        if(m_type == 1){
            return T_a()*2.0*PI*lambda_s()*h_a();
        }else if(m_type == 2){
            return T_a()*2.0*PI*lambda_i*h_a();
        }
    }

    double q_f(){
        if(m_type == 1){
            return q_s() + q_a();
        }else if (m_type == 2){
            return q_s() + q_a();
        }
    }

    double q_r(){
        if(m_type == 1){
            return q_s() - q_a();
        }else if(m_type == 2){
            return q_s() - q_a();
        }
    }

    double q(){
        if(m_type == 1){
            return q_f() + q_r();
        }else if(m_type == 2){
            return q_f() + q_r();
        }
    }

	//geteri
	int type() const { return m_type; }
	int Series() const { return m_Series; }
	int DN() const { return m_DN; }
	double CoverDepth() const { return m_CoverDepth; }
    double Z_k() const{
        return m_CreditInterest;
    }
    double R_i() const{
        return m_InflationRate;
    }

    double P_e() const{
        return m_PriceRiseDueToEnergy;
    }

    int n() const{
        return m_UsageTime;
    }

    double Q_pk() const{
        return m_ProductCosts;
    }

    int t() const{
        return m_TimeFactor;
    }
	
	//seteri
	void setType(int type) { m_type = type; }
	void setSeries(int Series) { m_Series = Series; }
	void setDN(int DN) { m_DN = DN; }
	void setCoverDepth(double CoverDepth) { m_CoverDepth = CoverDepth; }
	
protected:
	
	// Promenljive
	// type 1 - Single, 2 - Twin
	int m_type;
	int m_Series;
	int m_DN;
	double m_CoverDepth;
    int m_Number;
    int m_LengthTrasy;
    Wetness m_Wetness;
    double m_CreditInterest;
    double m_InflationRate;
    double m_PriceRiseDueToEnergy;
    int m_UsageTime;
    double m_ProductCosts;
    int m_TimeFactor;
 

    double CostPerMeter(){
        if(m_type == 1){
            if(m_Series == 1){
                return m_SinglePipes[m_DN].I();
            }else if(m_Series == 2){
                return m_SinglePipes[m_DN].II();
            }else if(m_Series == 3){
                return m_SinglePipes[m_DN].III();
            }
        }else if(m_type == 2){
            if(m_Series == 1){
                return m_TwinPipes[m_DN].I();
            }else if(m_Series == 2){
                return m_TwinPipes[m_DN].II();
            }
        }
    }
    
    double sum_ik(){
        if(m_type == 1){
            return 2*3.18*1.1*m_LengthTrasy*CostPerMeter();
        }else if(m_type == 2){
            return 2.8*1.07*m_LengthTrasy*CostPerMeter();
        }
    }

    double h_a(){
        return 1.0/h_a_inv();
    }

    double h_s(){
        return 1.0/h_s_inv();
    }

    double t_s(){
        return InstallTemp();
    }

    double lambda_s(){
        if (m_Wetness == dry){
            return 1.0;
        }else if (m_Wetness == medium){
            return 1.6;
        }else if(m_Wetness == wet)
            return 2.0;
    }

    double C(){
        if (m_type == 1) {
            if(m_Series == 1){
                return m_SinglePipes[m_DN].A()/1000.0 + m_SinglePipes[m_DN].Series1()/1000.0;
            }else if(m_Series == 2){
                return m_SinglePipes[m_DN].A()/1000.0 + m_SinglePipes[m_DN].Series2()/1000.0;
            }else if(m_Series == 3){
                return m_SinglePipes[m_DN].A()/1000.0 + m_SinglePipes[m_DN].Series3()/1000.0;
            }
        }else if (m_type == 2){
            return m_TwinPipes[m_DN].Lp()/1000.0 + m_TwinPipes[m_DN].Carrier()/1000.0;
        }
    }

    double beta(){
        return (lambda_s()/lambda_i)*log(D_i()/d_o());
    }

    double gamma(){
        return 2*(1- pow(sigma(), 2)) / (1 - sigma()* pow(D_i() / (4.0*Z_c()), 2));
    }

    double d_o(){
        return CarrierPipeOD()/1000.0;
    }

    double CarrierPipeOD(){
        if (m_type == 1){
            return m_SinglePipes[m_DN].Carrier();
        }else {
            return m_TwinPipes[m_DN].Carrier();
        }
    }

    double D_i(){
        return (CasingOD() - 2.0*sPEHD())/1000.0;
    }

    double Z_c(){
        return Z() + R_0*lambda_s();
    }

    double Z(){
        return CasingOD()/2000.0 + m_CoverDepth;
    }
    
	double CasingOD()
	{
		if (m_type == 1)
		{
			if (m_Series == 1)
				return m_SinglePipes[m_DN].Series1();
			else if (m_Series == 2)
				return m_SinglePipes[m_DN].Series2();
			return m_SinglePipes[m_DN].Series3();
		}
		else
		{
			if (m_Series == 1)
				return m_TwinPipes[m_DN].Series1();
			return m_TwinPipes[m_DN].Series2();
		}
	}

    double sPEHD()
	{
		if (m_type == 1)
		{
			if (m_Series == 1)
				return m_SinglePipes[m_DN].s1();
			else if (m_Series == 2)
				return m_SinglePipes[m_DN].s2();
			return m_SinglePipes[m_DN].s3();
		}
		else
		{
			if (m_Series == 1)
				return m_TwinPipes[m_DN].s1();
			return m_TwinPipes[m_DN].s2();
		}
	}

    double sigma(){
        return (lambda_i - lambda_s())/(lambda_i + lambda_s());
    }
    
};

#endif
