#ifndef PIPE_HPP
#define PIPE_HPP

class Pipe 
{
public:
    virtual ~Pipe() = default;
    
  	int DN() const { return m_DN; }
    double Carrier() const { return m_Carrier; }   
    double s() const { return m_s; }    
    int Series1() const { return m_Series1; }   
    int Series2() const { return m_Series2; }  
    double s1() const { return m_s1; } 
    double s2() const { return m_s2; }
    
    //povrsina poprecnog preseka                        --- kolona E
    virtual double SectionalArea() const = 0;
    
    //unutrasnji i lokalni naponi                       --- kolone I, J, K  
  	virtual double sigma_up() const = 0;
    virtual double sigma_lp() const = 0;
    virtual double t() const = 0;
    
    //kolicina vode u litrina po metru cevi             --- kolona P u tabeli ztraty
    virtual double water_per_meter() const = 0;

protected:
	Pipe() = default;
    Pipe(int DN, double Carrier, double s, int Series1, int Series2, double s1, double s2, double DesignWaterPressure)
        : m_DN(DN), m_Carrier(Carrier), m_s(s), m_Series1(Series1), m_Series2(Series2), m_s1(s1), m_s2(s2), m_DesignWaterPressure(DesignWaterPressure)
    {
    }
	
	//nominalni diametar                                --- kolona B
    int m_DN;
    //spoljni precnik celicne cevi                      --- kolona C
    double m_Carrier;
    //debljina zida celicne cevi                        --- kolona D
    double m_s;
    //precnici spoljne cevi za klase izolacije 1 i 2    --- kolone F, G
    int m_Series1;
    int m_Series2;
    //debljine zida plasticnih cevi                     --- kolone L, M
    double m_s1;
    double m_s2;
    //iz poject information sheeta
    double m_DesignWaterPressure;
};

#endif
