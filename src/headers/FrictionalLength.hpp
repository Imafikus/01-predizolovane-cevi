#ifndef FRICTIONALLENGTH_HPP
#define FRICTIONALLENGTH_HPP

#include <string>
#include <cmath>
#include <fstream>
#include <unordered_map>

#include "Pipe.hpp"
#include "SinglePipe.hpp"
#include "TwinPipe.hpp"
#include "Project.hpp"

class FrictionalLength : public Project
{
public:

	FrictionalLength(double FlowTemp, double ReturnTemp, double InstallTemp, double SoilDensity, double InternalSoilFrictionAngle,
                     double DesignWaterPressure, int type, int Series, int DN, double CoverDepth, 
                     std::string Name = "", std::string CalcRefNum = "", std::string CarriedOutBy = "", std::string Date = "")
		: Project(FlowTemp, ReturnTemp, InstallTemp, SoilDensity, InternalSoilFrictionAngle, DesignWaterPressure,
                  Name, CalcRefNum, CarriedOutBy, Date), m_type(type), m_Series(Series), m_DN(DN), m_CoverDepth(CoverDepth)
		{
        }

	~FrictionalLength() = default;
	
	// kolona AD
	double WeightForce()
	{
		double casing = CasingOD();
		double si = sPEHD();
		double carrier, sa, s;
		
		if (m_type == 1)
		{
			carrier = m_SinglePipes[m_DN].Carrier();
			sa = m_SinglePipes[m_DN].SectionalArea();
			s = m_SinglePipes[m_DN].s();
			
			double f1 = (casing*casing-4*casing*si+4*si*si-carrier*carrier)*PI/4*DensityPUR;
			double f2 = sa*DensitySteel+(casing-si)*PI*si*DensityPEHD;
			double f3 = (carrier*carrier-4*carrier*s-4*s*s)*PI/4*DensityWater;
			return (f1+f2+f3)/1000000*GravityCoeff;
		}
		else
		{
			carrier = m_TwinPipes[m_DN].Carrier();
			sa = m_TwinPipes[m_DN].SectionalArea();
			s = m_TwinPipes[m_DN].s();
			
			double f1 = (casing*casing-4*casing*si+4*si*si-2*carrier*carrier)*PI/4*DensityPUR;
			double f2 = sa*DensitySteel+(casing-si)*PI*si*DensityPEHD;
			double f3 = (carrier*carrier-4*carrier*s-4*s*s)*PI/2*DensityWater;
			return (f1+f2+f3)/1000000*GravityCoeff;
		}
	}
	
	//kolona AC
	double FrictionForce()
	{
		double casing = CasingOD();
		double wforce = WeightForce();
		
		double f1 = m_SoilDensity*GravityCoeff*(m_CoverDepth+casing/2000)*casing*(1+m_RestPressureCoeff)/2*PI;
		double f2 = wforce-m_SoilDensity*GravityCoeff*PI*casing*casing/4000000;
		return m_FrictionCoeff*(f1+f2);
	}
	
	//kolona AI
	double FrictionLength()
	{
		double fforce = FrictionForce();
		
		if (m_type == 1)
		{
			double sa = m_SinglePipes[m_DN].SectionalArea();
			return (m_Stress*sa)/fforce;
		}
		else
		{
			double sa = m_TwinPipes[m_DN].SectionalArea();
			return (sa*m_Stress-0.5*m_ThermalElongationCoeff*m_YoungsFlexibilityModulus*sa*m_TwinMeanTemp)/fforce;
		}
	}
	//geteri
	int type() const { return m_type; }
	int Series() const { return m_Series; }
	int DN() const { return m_DN; }
	double CoverDepth() const { return m_CoverDepth; }
	
	//seteri
	void setType(int type) { m_type = type; }
	void setSeries(int Series) { m_Series = Series; }
	void setDN(int DN) { m_DN = DN; }
	void setCoverDepth(double CoverDepth) { m_CoverDepth = CoverDepth; }
	
protected:
	
	// Promenljive
	// type 1 - Single, 2 - Twin
	int m_type;
	int m_Series;
	int m_DN;
	double m_CoverDepth;
	
	// Pomocne funkcije
	// kolona Z
	double CasingOD()
	{
		if (m_type == 1)
		{
			if (m_Series == 1)
				return m_SinglePipes[m_DN].Series1();
			else if (m_Series == 2)
				return m_SinglePipes[m_DN].Series2();
			return m_SinglePipes[m_DN].Series3();
		}
		else
		{
			if (m_Series == 1)
				return m_TwinPipes[m_DN].Series1();
			return m_TwinPipes[m_DN].Series2();
		}
	}
	// kolona AA
	double sPEHD()
	{
		if (m_type == 1)
		{
			if (m_Series == 1)
				return m_SinglePipes[m_DN].s1();
			else if (m_Series == 2)
				return m_SinglePipes[m_DN].s2();
			return m_SinglePipes[m_DN].s3();
		}
		else
		{
			if (m_Series == 1)
				return m_TwinPipes[m_DN].s1();
			return m_TwinPipes[m_DN].s2();
		}
	}
};

#endif
