# !usr/bin/bash

if [ -z "$1" ]
    then
        echo "Build folder path missing. Usage: bash generate_coverage_report.sh path/to/build/folder"
fi


SCRIPT_DIR=$(dirname $0)

BUILD_DIR=$1

COVERAGE_DIR="./coverage"

HTML_RESULTS="${COVERAGE_DIR}/html"

mkdir -p ${HTML_RESULTS}

#? Generate initial info
lcov -d "${BUILD_DIR}" -c -o "${COVERAGE_DIR}/coverage.info"

#? Leave only our code
lcov -r "${COVERAGE_DIR}/coverage.info" "*.h" "*/usr/include/*" "*/build*/*" -o "${COVERAGE_DIR}/coverage-filtered.info"

genhtml -o "${HTML_RESULTS}" "${COVERAGE_DIR}/coverage-filtered.info"

#? Cleanup
lcov -d "${COVERAGE_DIR}" -z

echo "index.html path (paste it into a browser):" $(readlink -f ./coverage/html/index.html)